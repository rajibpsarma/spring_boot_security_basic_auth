# Spring Boot security using Basic Authentication

It's a demo app having links to View Users, Add Users and Logout.
The link "Add Users" is protected using basic authentication.

It uses JSP as view. Bootstrap is used to style the pages.

Note: The logout functionality does not work properly here.

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/spring_boot_security_basic_auth.git
* cd spring_boot_security_basic_auth
* mvn clean spring-boot:run
* Now explore the app using "http://localhost:8080/"


