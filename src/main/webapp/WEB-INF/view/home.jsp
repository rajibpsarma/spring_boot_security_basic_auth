<!doctype HTML>
<html>
	<head>
		<title>Spring Security Demo</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
	</head>
	
	<body>
		<div class="container border border-round bg-light">
			<div class="row">
				<div class="col-12"><h5>Spring Security Basic Authentication Demo</h5><hr class="border-primary"></div>
			</div>
			<div class="row">
				<div class="col-12">Welcome ! <br>
				It demonstrates the use of basic authentication.<br><br>
				Here, View Users functionality is accessible to all.<br>
				Add user functionality is protected by using basic authentication.<br><br>
				
				
				</div>
			</div>
			<div class="row">
				<div class="col-3"><a href="/viewUsers">View Users</a></div>
				<div class="col-3"><a href="/addUsers">Add Users</a></div>
				<div class="col-3"><a href="#" onclick="logout()">Logout</a></div>
			</div>
		</div>
		<form method="post" action="/logout" name="logoutForm"></form>
		<script>
			function logout() {
				document.logoutForm.submit();
			}
		</script>
	</body>
</html>