<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Logout</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
		<%
		session.invalidate();
		%>
		<div class="container border border-round bg-light">
			<div class="row">
				<div class="col-12"><h5>You are logged out.</h5><hr class="border-primary"></div>
			</div><br>
			<div class="row">
				<div class="col-3"><a href="/">Home</a></div>
			</div>
		</div>
</body>
</html>