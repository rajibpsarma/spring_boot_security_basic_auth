<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Users</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
		<div class="container border border-round bg-light">
			<div class="row">
				<div class="col-12"><h5>Displaying the users in the system:</h5><hr class="border-primary"></div>
			</div>
			<div class="row">
				<div class="col-12">No record found</div>
			</div><br>
			<div class="row">
				<div class="col-3"><a href="/">Home</a></div>
			</div>
		</div>
</body>
</html>