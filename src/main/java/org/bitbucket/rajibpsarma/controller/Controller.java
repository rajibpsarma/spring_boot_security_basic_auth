package org.bitbucket.rajibpsarma.controller;

import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.stereotype.Controller
public class Controller {

	@GetMapping("/")
	public String showHomePage() {
		return "home";
	}
	
	@GetMapping("/viewUsers")
	public String showViewUsersPage() {
		return "viewUsers";
	}
	
	@GetMapping("/addUsers")
	public String showAddUsersPage() {
		return "addUsers";
	}
	
	// Redirected here once logged out
	@GetMapping("/loggedout")
	public String logout() {
		return "logout";
	}
}
