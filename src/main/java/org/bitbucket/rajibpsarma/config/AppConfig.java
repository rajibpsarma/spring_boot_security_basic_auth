package org.bitbucket.rajibpsarma.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class AppConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	// For web security, we need to override this method.
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.authorizeRequests()
			// Define what URLs are permitted without authentication
			.antMatchers("/", "/viewUsers", "/loggedout", "/css/*").permitAll()
			// Everything else needs authentication
			.anyRequest().authenticated()
			.and()
				.httpBasic()					// Use basic authentication
			// Implement logout functionality
			.and()
            	.logout() 						// Enable logout
            	.logoutUrl("/logout")			// The URL to trigger logout to happen
            	.logoutSuccessUrl("/loggedout")	// The URL to redirect to after logout is  done
            	.deleteCookies("JSESSIONID")
            	.clearAuthentication(true)
            	.invalidateHttpSession(true);
	}
}
